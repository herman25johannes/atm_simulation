# Source code and build jar
Source code is atm_simulation directory, build jar by running below command inside atm_simulation directory, and jar will be located in target directory.
$ mvn clean package

# How to run jar
Run this command targeting to jar location.
Example of command:
$ java -jar target/demo-1.0-SNAPSHOT.jar

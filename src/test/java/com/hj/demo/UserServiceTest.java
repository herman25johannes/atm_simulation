package com.hj.demo;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import com.hj.demo.service.*;

public class UserServiceTest {

    UserService userService = new UserService();

    @Test
    public void loginTest() {
        userService.login("Alice");
        assertEquals("Alice", userService.getLoggedinUser().getName());
    }

    @Test
    public void depositTest() {
        userService.login("Alice");
        userService.deposit("100");
        assertEquals(100, userService.getLoggedinUser().getBalance());
    }

    @Test
    public void logoutTest() {
        userService.login("Alice");
        userService.logout();
        assertEquals(null, userService.getLoggedinUser());
    }

    @Test
    public void transferTest() {
        userService.login("Alice");
        userService.logout();
        userService.login("Bob");
        userService.deposit("100");
        userService.transfer("Alice", "100");
        assertEquals(0, userService.getLoggedinUser().getBalance());
    }

    @Test
    public void transferOwedTest() {
        userService.login("Alice");
        userService.logout();
        userService.login("Bob");
        userService.deposit("100");
        userService.transfer("Alice", "200");
        assertEquals(0, userService.getLoggedinUser().getBalance());
        assertEquals(100, userService.getLoggedinUser().getOwedTo("Alice"));
    }

    @Test
    public void transferAndDepositTest() {
        userService.login("Bob");
        userService.logout();
        userService.login("Charlie");
        userService.logout();
        userService.login("Alice");
        userService.transfer("Bob", "100");
        userService.transfer("Charlie", "100");
        userService.deposit("150");
        assertEquals(0, userService.getLoggedinUser().getBalance());
        assertEquals(50, userService.getLoggedinUser().getOwedTo("Charlie"));
    }
}

package com.hj.demo;

import java.util.Scanner;

import com.hj.demo.constants.Command;
import com.hj.demo.service.*;

public class Main {

    public static void main(String[] args) throws Exception {
        /**
         * Assumption:
         * - deposit can only be integer value
         * - unknown command is ignored
         * - command with wrong or insufficient value will be ignored
         */

        UserService userService = new UserService();

        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                String line = scanner.nextLine();
                if (!line.isEmpty()) {
                    String[] input = line.trim().split(" ");

                    Command command;
                    command = Command.findByName(input[0]);
                    String value1 = "";
                    String value2 = "";

                    if (input.length > 1)
                        value1 = input[1].trim();

                    if (input.length > 2)
                        value2 = input[2].trim();

                    if (command == null) {

                        System.out.println("Please enter valid command [login, deposit, transfer, logout, exit]");

                    } else if (command.equals(Command.login)) {
                        userService.login(value1);
                    } else if (command.equals(Command.deposit)) {
                        userService.deposit(value1);
                    } else if (command.equals(Command.transfer)) {
                        userService.transfer(value1, value2);
                    } else if (command.equals(Command.logout)) {
                        userService.logout();
                    } else if (command.equals(Command.exit)) {
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("System close.");
        }
    }
}
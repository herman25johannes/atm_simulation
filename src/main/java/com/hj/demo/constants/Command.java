package com.hj.demo.constants;

public enum Command {
    login,
    deposit,
    withdraw,
    transfer,
    logout,
    exit;

    public static Command findByName(String name) {
        Command result = null;
        for (Command command : values()) {
            if (command.name().equalsIgnoreCase(name)) {
                result = command;
                break;
            }
        }
        return result;
    }
}

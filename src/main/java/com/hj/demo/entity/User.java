package com.hj.demo.entity;

import java.util.LinkedHashMap;
import java.util.Map;

public class User {
    private String name;
    private int balance;
    private Map<String, Integer> owedTo = new LinkedHashMap<>();

    public User() {};

    public User(String name, int balance) {
        this.name = name;
        this.balance = balance;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getBalance() {
        return this.balance;
    }

    public void setOwedTo(String name, int value) {
        this.owedTo.merge(name, value, Integer::sum);
    }

    public Map<String, Integer> getAllOwedTo() {
        return this.owedTo;
    }

    public int getOwedTo(String name) {
        return this.owedTo.get(name);
    }

    public void deposit(int value) {
        this.balance += value;
    }
}

package com.hj.demo.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hj.demo.entity.User;

public class UserService {

    // simulate database of users table
    private Map<String, User> users = new HashMap<>();

    // simulate logged in user
    private User loggedinUser;

    public void login(String name) {
        if (loggedinUser != null) {
            System.out.printf("You are already logged in as %s%n%n", loggedinUser.getName());
            return;
        }
        users.putIfAbsent(name, new User(name, 0));
        User user = users.get(name);
        loggedinUser = user;
        System.out.printf("Hello, %s!%n", user.getName());
        System.out.printf("Your balance is $%d%n%n", user.getBalance());
    }

    public void deposit(String value) {
        if (checkNotLoggedIn())
            return;
        try {
            int depositValue = Integer.parseInt(value);
            List<String> removedOwedUser = new ArrayList<>();

            // pay to owed user when deposit
            for (Map.Entry<String, Integer> entry : loggedinUser.getAllOwedTo().entrySet()) {
                if (depositValue > 0) {
                    final int owedValue = entry.getValue();
                    loggedinUser.getAllOwedTo().put(entry.getKey(), owedValue - depositValue);
                    if (depositValue <= owedValue) {
                        depositValue = 0;
                    } else {
                        depositValue -= owedValue;
                        removedOwedUser.add(entry.getKey());
                    }
                }
            }

            // remove owed user if already paid
            removedOwedUser.forEach(s -> {
                loggedinUser.getAllOwedTo().remove(s);
            });
            loggedinUser.deposit(depositValue);
            users.put(loggedinUser.getName(), loggedinUser);
            System.out.printf("Your balance is $%d%n", loggedinUser.getBalance());
            loggedinUser.getAllOwedTo().forEach((n, v) -> {
                System.out.printf("Owed $%d to %s%n", v, n);
            });
            System.out.println("");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Wrong deposit value, must be integer");
        }
    }

    public void transfer(String name, String value) {
        if (checkNotLoggedIn())
            return;
        User transferredUser = users.get(name);
        if (transferredUser == null) {
            System.out.printf("No user with %s name %n%n", name);
            return;
        }
        try {
            int transferValue = Integer.parseInt(value);
            int newBalance = loggedinUser.getBalance() - transferValue;
            if (loggedinUser.getBalance() - transferValue >= 0) {
                loggedinUser.setBalance(newBalance);
            } else {
                // set owed to if balance not sufficient
                loggedinUser.setBalance(0);
                loggedinUser.setOwedTo(name, Math.abs(newBalance));
                transferredUser.deposit(transferValue);
            }
            users.put(loggedinUser.getName(), loggedinUser);
            System.out.printf("Transferred $%d to %s %n", transferValue, name);
            System.out.printf("Your balance is $%d%n", loggedinUser.getBalance());
            loggedinUser.getAllOwedTo().forEach((n, v) -> {
                System.out.printf("Owed $%d to %s%n", v, n);
            });
            System.out.println("");
        } catch (Exception e) {
            System.out.println("Wrong transfer value, must be integer\n");
        }
    }

    public void logout() {
        if (checkNotLoggedIn())
            return;
        System.out.printf("Goodbye, %s!%n%n", loggedinUser.getName());
        loggedinUser = null;
    }

    public boolean checkNotLoggedIn() {
        if (loggedinUser == null) {
            System.out.println("You are not logged in, please login.");
            return true;
        }
        return false;
    }

    public User getLoggedinUser() {
        return this.loggedinUser;
    }
}
